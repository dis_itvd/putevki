'use strict';

$(function() {

    // SVG IE11 support
    svg4everybody();

    $('.raty').raty({
        number: 5,
        starHalf      : 'star-half',
        starOff       : 'star-off',
        starOn        : 'star-on',
        cancelOff     : 'cancel-of',
        cancelOn      : 'cancel-on',
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return $(this).attr('data-readOnly');
        },
    });

    $('.nav-toggle').on("click", function (e) {
        e.preventDefault();
        $('.page').toggleClass('nav-open');
    });

    $('.countries-toggle').on("click", function (e) {
        e.preventDefault();
        $('.countries').toggleClass('open');
    });

    var vendors = new Swiper('.vendors__slider', {
        slidesPerView: 'auto',
        direction: 'horizontal',
        freeMode: true,
        scrollbar: {
          el: '.vendors__scrollbar',
        },
        mousewheel: true,
    });
    
    var offer = new Swiper('.active .offer__slider', {
        loop: true,
        spaceBetween: 16,
        slidesPerView: 1,
        navigation: {
            nextEl: '.offer-next',
            prevEl: '.offer-prev',
        },
        breakpoints: {
            567: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 15,
            }
        }
    });

    $(".offer__nav--item").on("click", function (e)  {
        e.preventDefault();
        let tab = $(this).attr("data-tab");
        offer.destroy();

        $('.offer__nav--item').removeClass('active')
        $(this).toggleClass('active');

        $('.offer__tab').removeClass('active');
        $(tab).addClass('active');

        offer = new Swiper('.active .offer__slider', {
            loop: true,
            spaceBetween: 16,
            slidesPerView: 1,
            navigation: {
                nextEl: '.offer-next',
                prevEl: '.offer-prev',
            },
            breakpoints: {
                567: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                  slidesPerView: 3,
                  spaceBetween: 20,
                },
                1024: {
                  slidesPerView: 4,
                  spaceBetween: 15,
                }
            }
        });
    });
    
    var comments = new Swiper('.comments__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.comments-next',
            prevEl: '.comments-prev',
        },
        breakpoints: {
            768: {
              slidesPerView: 1,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 1,
              spaceBetween: 20,
            }
        }
    });
        
    var articles = new Swiper('.articles__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        breakpoints: {
            567: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 20,
            }
        }
    });
            
    var primary = new Swiper('.primary__slider', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.primary-next',
            prevEl: '.primary-prev',
        },
        breakpoints: {
            567: {
              slidesPerView: 1,
              spaceBetween: 10,
            },
            768: {
              slidesPerView: 1,
              spaceBetween: 30,
            }
        }
    });

    // History
    var historyPrimary = new Swiper('.history-slider-primary', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        navigation: {
            nextEl: '.history-next',
            prevEl: '.history-prev',
        },
    });

    var historyPrev = new Swiper('.history-slider-prev', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        shortSwipes: false,
        longSwipes: false,
        simulateTouch: false,
    });
    
    var historyNext = new Swiper('.history-slider-next', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 1,
        shortSwipes: false,
        longSwipes: false,
        simulateTouch: false,
    });

    historyPrimary.on('slidePrevTransitionStart', function () {
        historyPrev.slidePrev (300);
        historyNext.slidePrev (300);
    });
    historyPrimary.on('slideNextTransitionStart', function () {
        historyPrev.slideNext (300);
        historyNext.slideNext (300);
    });

});